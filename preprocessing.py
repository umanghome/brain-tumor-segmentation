import numpy as np
import subprocess
import random
import progressbar
import warnings
from glob import glob
from skimage import io
from nipype.interfaces.ants import N4BiasFieldCorrection

np.random.seed(5) # for reproducibility
progress = progressbar.ProgressBar(widgets=[progressbar.Bar('*', '[', ']'), progressbar.Percentage(), ' '])

class Brain(object):
	
	def __init__(self, path, n4itk = False):
		self.path = path
		self.n4itk = n4itk
		self.modes = ['flair', 't1', 't1c', 't2', 'gt']
		self.slices_by_mode, self.slices_by_slice = self.read_scans() # read .mha files
		self.normed_slices = self.norm_slices() # normalize slices

	def read_scans(self):
		print 'Loading scans...'
		slices_by_mode = np.zeros((5, 155, 240, 240)) # empty array
		slices_by_slice = np.zeros((155, 5, 240, 240)) # empty array

		# Paths
		flair = glob(self.path + '/*Flair*/*.mha') # flair
		t2 = glob(self.path + '/*_T2*/*.mha') # t2
		gt = glob(self.path + '/*more*/*.mha') # more
		t1s = glob(self.path + '/**/*T1*.mha') # t1 and t1c
		t1_n4 = glob(self.path + '/*T1*/*_n.mha') # normalized t1 and t1c
		# Get regular t1 and t1c
		t1 = [scan for scan in t1s if scan not in t1_n4]

		# default scans to use, may change if n4biascorrection is to be applied
		scans = [flair[0], t1[0], t1[1], t2[0], gt[0]] # directories to each image (5 total)

		# If n4biascorrection is to be applied
		if self.n4itk:
			if len(t1_n4) != len(t1): # Generate n4 bias corrected files if they don't exist
				print '-> Applyling bias correction...'
				for t1_path in t1:
					self.n4itk_norm(t1_path) # normalize files
				t1_n4 = glob(self.path + '/*T1*/*_n.mha') # normalized t1 and t1c			
			
			# Use n4 bias corrected scans
			scans = [flair[0], t1_n4[0], t1_n4[1], t2[0], gt[0]]

		# Generate slices
		for scan_idx in xrange(len(scans)):
			slices_by_mode[scan_idx] = io.imread(scans[scan_idx], plugin='simpleitk').astype(float)
		for mode_ix in xrange(slices_by_mode.shape[0]): # modes 1 thru 5
			for slice_ix in xrange(slices_by_mode.shape[1]): # slices 1 thru 155
				slices_by_slice[slice_ix][mode_ix] = slices_by_mode[mode_ix][slice_ix] # reshape by slice

		# Return slices
		return slices_by_mode, slices_by_slice

	def norm_slices(self):
		print 'Normalizing slices...'
		normed_slices = np.zeros((155, 5, 240, 240)) # Empty array

		# Perform operation on each slice
		for slice_ix in xrange(155):
			normed_slices[slice_ix][-1] = self.slices_by_slice[slice_ix][-1]
			for mode_ix in xrange(4):
				normed_slices[slice_ix][mode_ix] =  self._normalize(self.slices_by_slice[slice_ix][mode_ix])
		return normed_slices

	# Normalizes a slice
	def _normalize(self, slice):
		b, t = np.percentile(slice, (0.5,99.5))
		slice = np.clip(slice, b, t)
		if np.std(slice) == 0:
			return slice
		else:
			return (slice - np.mean(slice)) / np.std(slice)

	# Saves the PNGs of the Brain with given type
	def save_patient(self, reg_norm_n4, patient_num):
		print 'Saving scans for patient {}...'.format(patient_num)
		progress.currval = 0

		# If normalized images are required
		if reg_norm_n4 == 'norm': #saved normed slices
			for slice_ix in progress(xrange(155)): # reshape to strip
				strip = self.normed_slices[slice_ix].reshape(1200, 240)
				if np.max(strip) != 0: # set values < 1
					strip /= np.max(strip)
				if np.min(strip) <= -1: # set values > -1
					strip /= abs(np.min(strip))
				with warnings.catch_warnings():
					warnings.simplefilter("ignore")
					io.imsave('PNGs/norm/{}_{}.png'.format(patient_num, slice_ix), strip)
		# If regular images are required
		elif reg_norm_n4 == 'reg':
			for slice_ix in progress(xrange(155)):
				strip = self.slices_by_slice[slice_ix].reshape(1200, 240)
				if np.max(strip) != 0:
					strip /= np.max(strip)
				with warnings.catch_warnings():
					warnings.simplefilter("ignore")
					io.imsave('PNGs/reg/{}_{}.png'.format(patient_num, slice_ix), strip)
		# If n4 bias corrected images are required
		else:
			for slice_ix in progress(xrange(155)): # reshape to strip
				strip = self.normed_slices[slice_ix].reshape(1200, 240)
				if np.max(strip) != 0: # set values < 1
					strip /= np.max(strip)
				if np.min(strip) <= -1: # set values > -1
					strip /= abs(np.min(strip))
				with warnings.catch_warnings():
					warnings.simplefilter("ignore")
					io.imsave('PNGs/n4/{}_{}.png'.format(patient_num, slice_ix), strip)

	def n4itk_norm(self, path, n_dims = 3, n_iters = [20,20,10,5]):
		output_fn = path[:-4] + '_n.mha'
		n4bias(path, n_dims, n_iters, output_fn)
		# subprocess.call('python n4_bias_correction.py ' + path + ' ' + str(n_dims) + ' ' + n_iters + ' ' + output_fn, shell = True)

# Initialize each patient in a dataset
def init_patients (patients, n4itk):
	patients_list = []
	for patient_num, path in enumerate(patients):
		a = Brain(path, n4itk)
		patients_list.append(a)
	return patients_list

# Save PNGs of a type for a list of patients 
def save_patients (patients, type):
	for idx in xrange(len(patients_list)):
		patient = patients_list[idx]
		patient.save_patient(type, idx)

# Perform n4 bias correction
def n4bias (input_image, dimension, n_iterations, output_image):
	n4 = N4BiasFieldCorrection(output_image = output_image)
	n4.inputs.dimension = dimension
	n4.inputs.input_image = input_image
	n4.inputs.n_iterations = n_iterations
	n4.run()

if __name__ == '__main__':
	patients = glob('Dataset/HGG/**')
	patients_list = init_patients(patients, n4itk = False)
	save_patients(patients_list, 'reg')
	save_patients(patients_list, 'norm')
	save_patients(patients_list, 'n4')