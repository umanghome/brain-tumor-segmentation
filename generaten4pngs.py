import gc
import numpy as np
import subprocess
import random
import progressbar
import warnings
from glob import glob
from skimage import io
from nipype.interfaces.ants import N4BiasFieldCorrection

np.random.seed(5) # for reproducibility
progress = progressbar.ProgressBar(widgets=[progressbar.Bar('*', '[', ']'), progressbar.Percentage(), ' '])

class Brain(object):
	
	def __init__(self, path):
		self.path = path
		self.modes = ['flair', 't1', 't1c', 't2', 'gt']

	def read_scans(self):
		print 'Loading scans...'
		slices_by_mode = np.zeros((5, 155, 240, 240)) # empty array
		slices_by_slice = np.zeros((155, 5, 240, 240)) # empty array

		# Paths
		flair = glob(self.path + '/*Flair*/*.mha') # flair
		t2 = glob(self.path + '/*_T2*/*.mha') # t2
		gt = glob(self.path + '/*more*/*.mha') # more
		t1_n4 = glob(self.path + '/*T1*/*_n.mha') # normalized t1 and t1c

		# default scans to use, may change if n4biascorrection is to be applied
		scans = [flair[0], t1_n4[0], t1_n4[1], t2[0], gt[0]] # directories to each image (5 total)

		# Generate slices
		for scan_idx in xrange(len(scans)):
			slices_by_mode[scan_idx] = io.imread(scans[scan_idx], plugin='simpleitk').astype(float)
		for mode_ix in xrange(slices_by_mode.shape[0]): # modes 1 thru 5
			for slice_ix in xrange(slices_by_mode.shape[1]): # slices 1 thru 155
				slices_by_slice[slice_ix][mode_ix] = slices_by_mode[mode_ix][slice_ix] # reshape by slice

		gc.collect()
		# Return slices
		return slices_by_mode, slices_by_slice

	def norm_slices(self):
		print 'Normalizing slices...'
		normed_slices = np.zeros((155, 5, 240, 240)) # Empty array

		# Perform operation on each slice
		for slice_ix in xrange(155):
			normed_slices[slice_ix][-1] = self.slices_by_slice[slice_ix][-1]
			for mode_ix in xrange(4):
				normed_slices[slice_ix][mode_ix] =  self._normalize(self.slices_by_slice[slice_ix][mode_ix])

		gc.collect()
		return normed_slices

	# Normalizes a slice
	def _normalize(self, slice):
		b, t = np.percentile(slice, (0.5,99.5))
		slice = np.clip(slice, b, t)
		gc.collect()
		if np.std(slice) == 0:
			return slice
		else:
			return (slice - np.mean(slice)) / np.std(slice)

	# Saves the PNGs of the Brain with given type
	def save_patient(self, reg_norm_n4, save_path, patient_num):
		self.slices_by_mode, self.slices_by_slice = self.read_scans() # read .mha files

		if reg_norm_n4 == 'n4':
			self.normed_slices = self.slices_by_slice
		else:
			self.normed_slices = self.norm_slices() # normalize slices

		print 'Saving scans for patient {}...'.format(patient_num)
		progress.currval = 0

		for slice_ix in progress(xrange(155)): # reshape to strip
			strip = self.normed_slices[slice_ix].reshape(1200, 240)
			if np.max(strip) != 0: # set values < 1
				strip /= np.max(strip)
			if np.min(strip) <= -1: # set values > -1
				strip /= abs(np.min(strip))
			with warnings.catch_warnings():
				warnings.simplefilter("ignore")
				io.imsave(save_path + '/{}_{}.png'.format(patient_num, slice_ix), strip)
			gc.collect()

# Initialize each patient in a dataset
def init_patients (patients):
	patients_list = []
	for patient_num, path in enumerate(patients):
		a = Brain(path)
		patients_list.append(a)
	gc.collect()
	return patients_list

# Save PNGs of a type for a list of patients 
def save_patients (patients, save_path, type):
	ctr = 0
	length = len(patients_list)
	while ctr < length:
		patient = patients_list.pop(0)
		patient.save_patient(type, save_path, ctr)
		gc.collect()
		ctr += 1
#	for idx in xrange(len(patients_list)):
#		patient = patients_list[idx]
#		patient.save_patient(type, idx)
#		gc.collect()

def main ():
	patients = glob('Dataset/HGG/**')
	patients_list = init_patients(patients)
	save_patients(patients_list, 'PNGs/n4', 'n4')

if __name__ == '__main__':
	main()