import SimpleITK as sitk

'''
Converts a 3D .mha file to 155 slices of 2D images.
Inputs: filename, destination directory, and prefix for output file
Output: 155 files: destinationDirectory/namePrefix_#.png
'''

def mha3Dto2D (filename, destinationDirectory, namePrefix):
	image = sitk.ReadImage(filename) # Read .mha file
	arr = sitk.GetArrayFromImage(image) # Get array of 155 images
	caster = sitk.CastImageFilter()
	caster.SetOutputPixelType(13) # 13 is pixelID for PNG
	for idx in xrange(len(arr)): # Iterate over all 155 images
		img = arr[idx] # i-th image
		img = sitk.GetImageFromArray(img) # Convert array to image
		img = caster.Execute(img) #Convert to PNG
		destination = destinationDirectory + namePrefix + '_' + str(idx) + '.png' # Destination name
		sitk.WriteImage(img, destination) # Write i-th image
		
# mha3Dto2D('Dataset/HGG/brats_2013_pat0001_1/VSD.Brain.XX.O.MR_T1.54513/VSD.Brain.XX.O.MR_T1.54513.mha', 'Labels/', 'sitk')
