import progressbar
import warnings
from glob import glob
from skimage import io

progress = progressbar.ProgressBar(widgets=[progressbar.Bar('*', '[', ']'), progressbar.Percentage(), ' '])

def save_labels (files):
  with warnings.catch_warnings():
    warnings.simplefilter('ignore')
    progress.currval = 0
    for label_idx in progress(xrange(len(files))):
      slices = io.imread(files[label_idx], plugin = 'simpleitk')
      for slice_idx in xrange(len(slices)):
        io.imsave('PNGs/label/{}_{}L.png'.format(label_idx, slice_idx), slices[slice_idx])

def main ():
  labels = glob('Dataset/HGG/**/*more*/**.mha')
  save_labels(labels)

if __name__ == '__main__':
  main()
