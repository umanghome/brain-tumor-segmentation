import numpy as np
import random
import os
from glob import glob
from skimage import io
import progressbar
from PIL import Image
from matplotlib import pyplot as plt

progress = progressbar.ProgressBar(widgets=[progressbar.Bar('*', '[', ']'), progressbar.Percentage(), ' '])
np.random.seed(5)

class PatchLibrary (object):
  def __init__ (self, patch_size, train_data, num_samples):
    self.patch_size = patch_size # size of patches to extract
    self.train_data = train_data # list of PNGs
    self.num_samples = num_samples # number of patches to return
    self.h = patch_size[0]
    self.w = patch_size[1]

  # Generates training patches.
  def generate_training_patches (self):
    return self.make_training_patches_randomly()

  # Calls find_patches which picks random files and generates patches
  def make_training_patches_randomly (self, entropy = False, classes = [0, 1, 2, 3, 4]):
    per_class = self.num_samples / len(classes) # how many patches per class
    patches, labels = [], [] # initialse to empty arrays
    progress.currval = 0
    for i in progress(xrange(len(classes))): # loop for all classes
      p, l = self.find_patches(classes[i], per_class) # find per_class number of patches for class classes[i]
      for img_ix in range(len(p)): # for each patch
        for slice in xrange(len(p[img_ix])): # normalize each slice by dividing with the max value of the slice
          if np.max(p[img_ix][slice]) != 0:
            p[img_ix][slice] /= np.max(p[img_ix][slice])
      patches.append(p) # append patch
      labels.append(l) # append label
    return np.array(patches).reshape(self.num_samples, 4, self.h, self.w), np.array(labels).reshape(self.num_samples) # reshape arrays and return

  # Finds patches of a given class by picking random files and returns the patches
  def find_patches (self, class_num, num_patches):
    h, w = self.patch_size[0], self.patch_size[1] # height and width
    patches = [] # empty list
    labels = np.full(num_patches, class_num, 'float') # fill the list with num_patches number of elements, where each element has the value class_num
    print 'Finding patches of class {}..'.format(class_num)

    ct = 0 # counter
    while ct < num_patches: # loop until we get enough patches
      im_path = random.choice(self.train_data) # get random image from train_data
      fn = os.path.basename(im_path) # get the file name of the randomly selected image
      label = io.imread('PNGs/label/' + fn[:-4] + 'L.png') # read the label file of the selected image

      # if there are not enough labels of classes classes[i] in the file, skip
      if len(np.argwhere(label == class_num)) < 10:
        continue

      img = io.imread(im_path).reshape(5, 240, 240)[:-1].astype('float') # read randomly selected image from train_data
      p = random.choice(np.argwhere(label == class_num)) # get a random pixel from the image. This will be the center pixel for the patch
      p_ix = (p[0] - (h / 2), p[0] + ((h + 1) / 2), p[1] - (w / 2), p[1] + ((w + 1) / 2)) # get patch boundaries
      patch = np.array([i[p_ix[0]:p_ix[1], p_ix[2]:p_ix[3]] for i in img]) # create array of pixels within the patch boundary

      # if the shape isn't 4 x height x widhth, or if the patch cotains more than 25% empty values, skip
      if patch.shape != (4, h, w) or len(np.argwhere(patch == 0)) > (h * w):
        continue

      patches.append(patch) # append patch
      ct += 1 # increment counter
    return np.array(patches), labels # return patches and labels

# method to show patches
def showPatches (patches):
  for i in xrange(len(patches)):
    for j in xrange(len(patches[i])):
      plt.imshow(patches[i][j])
      plt.show()

def main ():
  train_data = glob('PNGs/n4/**') # get training data
  patches = PatchLibrary((33, 33), train_data, 50) # initialize the object
  x, y = patches.generate_training_patches() # get patches and labels
  # showPatches(x)
  # z = x[0]
  # for i in xrange(len(z)):
    # plt.imshow(z[i])
    # plt.show()
  # showPatches(x)

if __name__ == '__main__':
  main()