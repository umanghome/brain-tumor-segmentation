import gc
import generaten4correctedfiles as GenN4Corrected
import generaten4pngs as GenN4PNGs
from glob import glob
import sys

def main ():
  if len(sys.argv) != 2:
    print 'Usage: processtestdata.py <Directory Path>'
    sys.exit(0)

  path = sys.argv[1]

  # Generate N4 Bias Corrected MHA file
  t1s = glob(path + '/**/*T1*.mha') # t1 and t1c
  t1_n4 = glob(path + '/*T1*/*_n.mha') # normalized t1 and t1c
  t1 = [scan for scan in t1s if scan not in t1_n4]
  if len(t1_n4) != len(t1): # Generate n4 bias corrected files if they don't exist
      print '-> Applyling bias correction...'
      for t1_path in t1:
        GenN4Corrected.n4itk_norm(t1_path) # normalize files
      t1_n4 = glob(path + '/*T1*/*_n.mha') # normalized t1 and t1c
  gc.collect()

  # Generate PNGs
  patient = GenN4PNGs.Brain(path)
  patient.save_patient('n4', 'tmp/pngs/', 0)
  gc.collect()

if __name__ == '__main__':
  main()