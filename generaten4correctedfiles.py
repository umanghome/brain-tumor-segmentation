import gc
import numpy as np
import subprocess
import random
import progressbar
import warnings
from glob import glob
from skimage import io
from nipype.interfaces.ants import N4BiasFieldCorrection

def n4itk_norm(path, n_dims = 3, n_iters = [20,20,10,5]):
  output_fn = path[:-4] + '_n.mha'
  n4bias(path, n_dims, n_iters, output_fn)
  gc.collect()

def n4bias (input_image, dimension, n_iterations, output_image):
  n4 = N4BiasFieldCorrection(output_image = output_image)
  n4.inputs.dimension = dimension
  n4.inputs.input_image = input_image
  n4.inputs.n_iterations = n_iterations
  n4.run()
  gc.collect()

def main ():
  patients = glob('Dataset/HGG/**')
  for idx in xrange(len(patients)):
    path = patients[idx]
    t1s = glob(path + '/**/*T1*.mha') # t1 and t1c
    t1_n4 = glob(path + '/*T1*/*_n.mha') # normalized t1 and t1c
    t1 = [scan for scan in t1s if scan not in t1_n4]
    if len(t1_n4) != len(t1): # Generate n4 bias corrected files if they don't exist
        print '-> Applyling bias correction...'
        for t1_path in t1:
          n4itk_norm(t1_path) # normalize files
        t1_n4 = glob(path + '/*T1*/*_n.mha') # normalized t1 and t1c
    gc.collect()

if __name__ == '__main__':
  main()
