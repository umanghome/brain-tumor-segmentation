import helpers as Helpers
import numpy as np
from skimage import io
import sys
import warnings

def main ():
  if len(sys.argv) != 4 and len(sys.argv) != 5:
    print 'Usage: python overlayimage.py <Path to Original Image> <Path to Labels> <Mode> <Path to Output Image | Optional>'
    sys.exit(0)

  original_path = sys.argv[1]
  label_path = sys.argv[2]
  output_path = sys.argv[3]

  # labels = io.imread(label_path)

  if len(sys.argv) == 4:
    output_image = Helpers.overlay_image_on_other(label_path, original_path)
  elif len(sys.argv) == 5:
    output_image = Helpers.overlay_image_on_other(label_path, original_path, mode = str(sys.argv[4]))

  with warnings.catch_warnings():
    warnings.simplefilter('ignore')
    io.imsave(output_path, output_image)

if __name__ == '__main__':
  main()
