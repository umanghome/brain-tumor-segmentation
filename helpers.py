import numpy as np
from skimage import color, io, img_as_float
from skimage.exposure import adjust_gamma

def get_x_y_to_fit (x_train, y_train):
  shuffle = zip(x_train, y_train)
  np.random.shuffle(shuffle)
  x = np.array([shuffle[i][0] for i in xrange(len(shuffle))])
  y = np.array([shuffle[i][1] for i in xrange(len(shuffle))])
  return x, y

def get_patches_to_predict (model, test_img):
  imgs = io.imread(test_img).astype('float').reshape(5,240,240)
  plist = []
  for imt in imgs[:-1]:
    if np.max(img) != 0:
      img /= np.max(img)
    p = extract_patches_2d(img, (33, 33))
    plist.append(p)
  patches = np.array(zip(np.array(plist[0]), np.array(plist[1]), np.array(plist[2]), np.array(plist[3])))
  # full_pred = model.predict(patches)
  # fp1 = full_pred.reshape(208, 208)
  # return np.pad(fp1, (16,16), mode='edge')
  return patches

def pad_predicted_image (pred):
  pr = pref.reshape(208, 208)
  return np.pad(pr, (16, 16), mode='edge')

def overlay_segmented_image_on_other (segmented, other):
  img_mask = segmented
  ones = np.argwhere(img_mask == 1)
  twos = np.argwhere(img_mask == 2)
  threes = np.argwhere(img_mask == 3)
  fours = np.argwhere(img_mask == 4)

  gray_img = img_as_float(other)
  image = adjust_gamma(color.gray2rgb(gray_img), 0.65)
  sliced_image = image.copy()
  red_multiplier = [1, 0.2, 0.2]
  yellow_multiplier = [1,1,0.25]
  green_multiplier = [0.35,0.75,0.25]
  blue_multiplier = [0,0.25,0.9]

  for i in xrange(len(ones)):
    sliced_image[ones[i][0]][ones[i][1]] = red_multiplier
  for i in xrange(len(twos)):
    sliced_image[twos[i][0]][twos[i][1]] = green_multiplier
  for i in xrange(len(threes)):
    sliced_image[threes[i][0]][threes[i][1]] = blue_multiplier
  for i in xrange(len(fours)):
    sliced_image[fours[i][0]][fours[i][1]] = yellow_multiplier

  return sliced_image

def overlay_segmented_image_on_original (segmented, original, mode = 't1c'):
  modes = {'flair':0, 't1':1, 't1c':2, 't2':3}
  original = io.imread(original)
  original = original.reshape(5,240,240)
  image = original[modes[mode]]
  return overlay_segmented_image_on_other(segmented, image)

def overlay_image_on_other (mask_path, original, mode = 't1c'):
  mask = io.imread(mask_path)
  return overlay_segmented_image_on_original(mask, original, mode)
